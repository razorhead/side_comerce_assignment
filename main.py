import sys, csv
import pymysql.cursors

# database keys
ID_KEY = "id"
NAME_KEY = "name"
PARENT_KEY = "parent"
CODE_KEY = "code"
# season column keys
SEASON_NAME_KEY = "SEASON"
SEASON_CODE_KEY = "SEASON CODE"
# color column keys
COLOR_NAME_KEY = "COLOR"
COLOR_CODE_KEY = "COLOR CODE"
# product column keys
PRODUCT_UPC_KEY = "UPC"
PRODUCT_NAME_KEY = "NAME"
PRODUCT_SKU_KEY = "SKU"
PRODUCT_TAX_KEY = "TAX"
PRODUCT_PRICE_KEY = "PRICE"
PRODUCT_SIZE_KEY = "SIZE"
PRODUCT_ONHAND_KEY = "ONHAND"
PRODUCT_DESCRIPTION_KEY = "DESCRIPTION"
PRODUCT_SALE_PRICE_KEY = "SALE PRICE"
PRODUCT_ON_SALE_KEY = "ON SALE"
# mysql connection
connection = pymysql.connect(
    host=sys.argv[1],
    user=sys.argv[2],
    password=sys.argv[3],
    db=sys.argv[4],
    cursorclass=pymysql.cursors.DictCursor)
current_categories = []
current_seasons = []
current_colors = []


def main():
    """parse the CSV file and create new products"""
    with open('data/feed.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # trim the strings in the row
            row = {k: v.strip() for k, v in row.items()}
            # store the product categories into row_categories
            row_categories = {k: v for k, v in row.items() if k.startswith("CATEGORY ")}
            category_id = create_categories(row_categories)
            season_id = create_season(row)
            color_id = create_color(row)
            if not create_product(row, category_id, season_id, color_id):
                sys.exit(0)

    # Finally commit all pending transactions
    try:
        connection.commit()
    finally:
        connection.close()


def create_product(row, category_id, season_id, color_id):
    """Insert a new product into products table.
    Args:
        row (dict): CSV file row containing the product detail.
        category_id (int): the category id that the product belongs to.
        season_id (int): the season id that the product belongs to.
        color_id (int): the color id that the product belongs to.
    Return:
        bool: True if the transaction was successful, False otherwise
    """
    product_parameters = (
        row[PRODUCT_UPC_KEY],
        row[PRODUCT_SKU_KEY],
        row[PRODUCT_NAME_KEY],
        1 if row[PRODUCT_TAX_KEY] == "Y" else 0,
        float(row[PRODUCT_PRICE_KEY]),
        row[PRODUCT_SIZE_KEY],
        int(row[PRODUCT_ONHAND_KEY]),
        None if not row[PRODUCT_DESCRIPTION_KEY] else row[PRODUCT_DESCRIPTION_KEY],
        1 if row[PRODUCT_ON_SALE_KEY] == "Y" else 0,
        None if not row[PRODUCT_SALE_PRICE_KEY] else float(row[PRODUCT_SALE_PRICE_KEY]),
        int(category_id),
        int(season_id),
        int(color_id),
    )
    try:
        with connection.cursor() as cursor:
            new_product_sql = """ INSERT INTO `products` (`upc`, `sku`, `name`, `tax`, `price`, `size`, `quantity`, 
                                `description`, `on_sale`, `sale_price`, `category_id`, `season_id`, `color_id`) 
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
            cursor.execute(new_product_sql, product_parameters)
            cursor.close()
            return True
    except Exception:
        # We can check for other kinds of exceptions(IntegrityError, OperationalError, ...) here as well.
        # But for the sake of simplicity we just let the user know there was a problem
        print("There was a problem inserting a new product")
        return False


def create_categories(row_categories):
    """Get the color id to associate with the product.
        Args:
            row (dict): CSV file row containing the product detail.
        Return:
            int: the category id associated with the product to be inserted
    """
    category_id = None
    for category_key, category_value in row_categories.items():
        if category_id is None:  # We haven’t created a parent category yet
            existing_category = next((item for item in current_categories if item[NAME_KEY] == category_value), None)
            if existing_category is not None:  # reuse the existing category if already exists
                category_id = existing_category[ID_KEY]
            else:
                category_id = insert_category(category_value)
        elif not category_value:  # category value is an empty string. Break out of the loop and use the existing category_id as product's category_id
            break
        else:  # parent category is already created with id of category_id
            existing_child_category = next((item for item in current_categories if item[NAME_KEY] == category_value), None)
            if existing_child_category is not None:  # reuse the existing category if not exists
                category_id = existing_child_category[ID_KEY]
            else:
                category_id = insert_category(category_value, category_id)
    # return the last computed category to be used as the product's category
    return category_id


def insert_category(name, parent = None):
    """Get the color id to associate with the product.
        Args:
            name (str): Name of the category to be inserted
            parent (int/None): the parent category id
        Return:
            int: the id of the newly created category
    """
    try:
        with connection.cursor() as cursor:
            new_category_sql = "INSERT INTO `categories` (`name`, `parent`) VALUES (%s, %s)"
            # the last computed category_id is THE parent category
            cursor.execute(new_category_sql, (name, parent))
            category_id = cursor.lastrowid
            current_categories.append(
                {ID_KEY: category_id, NAME_KEY: name, PARENT_KEY: parent})
            cursor.close()
            return category_id
    except Exception:
        print("There was a problem creating a new category")
    sys.exit(0)


def create_season(row):
    """Get the season id to associate with the product.
        Args:
            row (dict): CSV file row containing the product detail.
        Return:
            int: the season id associated with the product to be inserted
        Notes:
            Please note that we could have merged this function with create_color not to avoid writing duplicate code but
            for sake of simplicity, we keep them separate
    """
    name, code = row[COLOR_NAME_KEY], row[COLOR_CODE_KEY]
    existing_color = next((item for item in current_seasons if item[CODE_KEY] == code), None)
    if existing_color is not None:  # reuse the existing season id if exist
        return existing_color[ID_KEY]
    else:
        try:
            with connection.cursor() as cursor:
                new_color_sql = "INSERT INTO `seasons` (`name`, `code`) VALUES (%s, %s)"
                cursor.execute(new_color_sql, (name, code))
                color_id = cursor.lastrowid
                current_seasons.append({ID_KEY: color_id, NAME_KEY: name, CODE_KEY: code})
                cursor.close()
                return color_id
        except Exception:
            print("There was a problem inserting a new season")
        sys.exit(0)

def create_color(row):
    """Get the color id to associate with the product.
        Args:
            row (dict): CSV file row containing the product detail.
        Return:
            int: the color id associated with the product to be inserted
        Notes:
            Please note that we could have merged this function with create_season not to avoid writing duplicate code but
            for sake of simplicity, we keep them separate
    """
    name, code = row[SEASON_NAME_KEY], row[SEASON_CODE_KEY]
    existing_season = next((item for item in current_colors if item[CODE_KEY] == code), None)
    if existing_season is not None:  # reuse the existing color id if exist
        return existing_season[ID_KEY]
    else:
        try:
            with connection.cursor() as cursor:
                new_season_sql = "INSERT INTO `colors` (`name`, `code`) VALUES (%s, %s)"
                cursor.execute(new_season_sql, (name, code))
                season_id = cursor.lastrowid
                current_colors.append({ID_KEY: season_id, NAME_KEY: name, CODE_KEY: code})
                cursor.close()
                return season_id
        except Exception:
            print("There was a problem inserting a new color")
        sys.exit(0)

if __name__ == '__main__':
    main()
