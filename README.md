# SideCommerce Assignment

This repository contains the code for the requested e-commerce assignment.

  - ***What problems, if any, do you spot with this data?***
    * Category hierarchy is unknown. I was able to guess the correct hierarchy.
    * There's a Boys category under the Snow category. Since there already exists a Boys category, the two categories must be switched out. I made that change to the original data because I believe that the data must be modified properly so the code written to parse can be reusable in the future with minimal changes.
    * Some of the descriptions contain `-` character. I again changed those to empty string because if we were to display the description on a web page, it doesn't make sense to only add a character.
    * Maybe different Seasons could be another category. This way we can use the same logic to filter out the the products associated with a specific season.
    * There is a missing tax rate. This is usually determined by the locations of buyer and seller

- ***What data structures would you use to represent this data and how are they related to each other?***
    * I would use a dictionary (JSON) as a data structure for a faster lookup. Becasue each product has it's unique combination of category, season, and color it is best to keep a reference of the those data in the product.
- ***Why did you choose the solution you came up with, and are there any alternatives you considered?***
    * parsing CSV and inserting the records one by one seemed like the right thing to do for the data provided. This could change if there was a lot more data. I was thinking about doing a bulk insert as an alternative but though that it is best to make transactions and commit them all at the end. For larger data we must have take precaution to make sure we take the right approach to ensure all transactions are commited in the right order and successfully. Even though the code in this repository ensures of that, I am sure there are other handling could be applied.

### Installation

Simply run the following command in terminal

```sh
$ python main.py "db-host" "db-username" "db-password" "db-name"
```
Note: `PyMySQL` package is required to run this application.



